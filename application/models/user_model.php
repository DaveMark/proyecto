<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
    
    public function retornarusuarios()
    {
        $this->db->SELECT('idUsuario,login,password,tipo,estado');
        $this->db->FROM('usuarios');
     //   $this->db->WHERE('estado',$estado);
        return $this->db->get();
    }



    public function insertarUsuario($data)
    {
        $this->db->INSERT('usuarios',$data);
    }


    public function recuperarUsuario($id)
    {
        $this->db->SELECT('idUsuario,login,tipo');
        $this->db->FROM('usuarios');
        $this->db->WHERE('idUsuario',$id);
        return $this->db->get();
    }

    public function modificarUsuario($id,$data)
    {
        $this->db->WHERE('idUsuario',$id);
        $this->db->UPDATE('usuarios',$data);
    }
    public function eliminarUsuario($id,$data)
    {
        $this->db->WHERE('idUsuario',$id);
        $this->db->UPDATE('usuarios',$data);
    }
    public function insertarConductor($data)
    {
        $this->db->INSERT('conductor',$data);
    }

}

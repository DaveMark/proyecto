<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehiculo_model extends CI_Model {
    
    public function retornarVehiculo()
    {
        $estado=1;
        $this->db->SELECT('idVehiculo,marca,modelo,color,placa,anioFabricacion,imagen');
        $this->db->FROM('vehiculo');
        $this->db->WHERE('estado',$estado);
        return $this->db->get();
    }
    public function recuperarVehiculo($id)
    {
        $this->db->SELECT('idVehiculo,marca,modelo,color,placa,anioFabricacion,imagen');
        $this->db->FROM('vehiculo');
        $this->db->WHERE('idVehiculo',$id);
        return $this->db->get();
    }
    public function modificarVehiculo($id,$data)
    {
        $this->db->WHERE('idVehiculo',$id);
        $this->db->UPDATE('vehiculo',$data);
    }
    public function eliminarVehiculo($id,$data)
    {
        $this->db->WHERE('idVehiculo',$id);
        $this->db->UPDATE('vehiculo',$data);
    }
    public function insertarVehiculo($data)
    {
        $this->db->INSERT('vehiculo',$data);
    }

}

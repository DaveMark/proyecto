<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conductor_model extends CI_Model {
    
    public function retornarConductor()
    {
        $estado=1;
        $this->db->SELECT('idConductor,ci,primerApellido,segundoApellido,nombres,direccion,telefono,estado,imagen');
        $this->db->FROM('conductor');
        $this->db->WHERE('estado',$estado);
        return $this->db->get();
    }
    public function recuperarConductor($id)
    {
        $this->db->SELECT('idConductor,ci,primerApellido,segundoApellido,nombres,direccion,telefono,imagen');
        $this->db->FROM('conductor');
        $this->db->WHERE('idConductor',$id);
        return $this->db->get();
    }
    public function modificarConductor($id,$data)
    {
        $this->db->WHERE('idConductor',$id);
        $this->db->UPDATE('conductor',$data);
    }
    public function eliminarConductor($id,$data)
    {
        $this->db->WHERE('idConductor',$id);
        $this->db->UPDATE('conductor',$data);
    }
    public function insertarConductor($data)
    {
        $this->db->INSERT('conductor',$data);
    }

}

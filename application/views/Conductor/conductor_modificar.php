<div class="container">
		
      <!-- Main component for a primary marketing message or call to action -->
      <div class="row">
 
	   
		 <div class="col-md-7">
         <h3 ><span class="fas fa-edit"></span> Modificar Conductor</h3>
         <?php 
      foreach ($conductor->result() as $row) {
      ?>
      
          
			<form action="<?=base_url()?>index.php/conductor/modificardb" class="form-horizontal" method="POST" id="formConductor" enctype="multipart/form-data">
                
            
            
				  <input type="hidden" class="form-control" id="idConductor" name="idConductor" value="<?php echo $row->idConductor; ?>" >
				  
				
			 
			  
			  <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label">CI</label>
				<div class="col-sm-9">
				  <input type="text" class="form-control" id="ci" name="ci"  maxlength="15"  value="<?php echo $row->ci; ?>" required >
				  
				</div>
			  </div>
			  
			  <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label" >Nombres</label>
				<div class="col-sm-9">
                <input type="text" class="form-control" id="nombres" name="nombres" maxlength="15"  value="<?php echo $row->nombres; ?>  " required >
				</div>
              </div>
              <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label">Primer Apellido</label>
				<div class="col-sm-9">
                <input type="text" class="form-control" id="primerapellido" name="primerapellido" maxlength="15"  value="<?php echo $row->primerApellido; ?> " required >
				</div>
              </div>	
              <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label">Segundo Apellido</label>
				<div class="col-sm-9">
                <input type="text" class="form-control" id="segundoapellido" name="segundoapellido" maxlength="15"  value="<?php echo $row->segundoApellido; ?> " >
				</div>
              </div>	
              <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label">Direccion</label>
				<div class="col-sm-9">
                <input type="text" class="form-control" id="direccion" name="direccion" maxlength="30"  value="<?php echo $row->direccion; ?>" required>
				</div>
              </div>	
              <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label">Telefono</label>
				<div class="col-sm-9">
                <input type="text" class="form-control" id="telefono" name="telefono" maxlength="8"  value="<?php echo $row->telefono; ?>" required>
				</div>
			  </div>	
			  
			
			  
			  
			  
			
			 
			  
			  
			  <div class="form-group">
			  <div id='loader'></div>
			  <div class='outer_div'></div>
				<div class="col-sm-offset-3 col-sm-9">
				  <button type="submit" class="btn btn-warning" id="enviarConductor">Modificar datos</button>
				</div>
			  </div>
		
			
			
			
		</div>
		<div class="col-md-5">
		 <h3 ><span class="	fas fa-file-image"></span> Fotografia</h3>
		 
		
		 
			<div class="form-group">
				
				<div class="col-sm-12">
				
				 
				 <div class="fileinput fileinput-new" data-provides="fileinput">
								  <div class="fileinput-new thumbnail" style="max-width: 100%;" >
									  <img class="img-rounded" src="<?=base_url()?>/uploads/imagenes_conductor/<?php echo $row->imagen;?> " style="max-width: 250px; max-height: 250px;" />
								  </div>
								  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 250px; max-height: 250px;">
										
										
									</div>
								  <div>
									<span class="btn btn-warning btn-file"><span class="fileinput-new">Selecciona una imagen</span>
									<span class="fileinput-exists" onclick="upload_image();">Cambiar imagen</span><input type="file" value="<?php echo $row->imagen;?> " name="fileToUpload" id="fileToUpload" onchange="upload_image();"></span>
									<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Cancelar</a>
								  </div>
					</div>
				
					
				</div>
				
			  </div>
              
	

				<div id="alerta" class="col-sm-9">
            
						</div>
			
			  
			 
			  
			  
         </form>
				 <!-- Custom scripts for validation Driver-->
  			 <script src="<?=base_url()?>/bootstrap/js/validarConductor.js"></script>
         <!--h1>Selecciona un dispositivo</h1>
	<div>
		<select name="listaDeDispositivos" id="listaDeDispositivos"></select>
		<button id="boton">Tomar foto</button>
		<p id="estado"></p>
	</div>
	<br>
	<video muted="muted" id="video"></video>
	<canvas id="canvas" style="display: none;"></canvas>
         
		</div-->

    </div> 
    <?php
      
      
      }
      ?>
    </div><!-- /container -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    


    
<!-- Begin Page Content -->
<div class="container-fluid">


<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-warning">Lista de Conductores</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>N°</th>
            <th>Ci</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Nombres</th>
            <th>Dirección</th>
            <th>Telefono</th>
            
            <th>Modificar</th>
            <th>Eliminar</th>
          </tr>
        </thead>
        <tbody>
        <?php $indice=1 ;
      foreach ($conductores->result() as $row) {
      ?>
      <tr>
        <td><?php echo $indice;?></td>
        <td><?php echo $row->ci;?></td>
        <td><?php echo $row->primerApellido;?></td>
        <td><?php echo $row->segundoApellido;?></td>
        <td><?php echo $row->nombres;?></td>
        <td><?php echo $row->direccion;?></td>
        <td><?php echo $row->telefono;?></td>
        
        

        <td>
          <?php echo form_open_multipart('conductor/modificar');?>
            <input type="hidden" name="idConductor" value="<?php echo $row->idConductor;?>">
            
           
            <button type="submit" class="btn btn-warning btn-icon-split">
            <span class="icon text-white-50">
                      <i class="fas fa-exclamation-triangle"></i>
                    </span>
                    <span class="text">Modificar</span></button>
          <?php echo form_close(); ?>
        </td>
        <td>
          <?php echo form_open_multipart('conductor/eliminardb');?>
            <input type="hidden" name="idConductor" value="<?php echo $row->idConductor;?>">
            
                  
                
                  
            <button type="submit" class="btn btn-danger btn-icon-split">
            <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Eliminar</span></button>
          <?php echo form_close(); ?>
        </td>

      <?php
      $indice++;
      
      }
      ?>
      
     
    
    
        </tbody>
         
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
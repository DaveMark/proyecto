<div class="container">
		
      <!-- Main component for a primary marketing message or call to action -->
      <div class="row">
 
	   
		 <div class="col-md-7">
		 <h3 ><span class="fas fa-edit"></span> Registrar Conductor</h3>
			<form action="<?=base_url()?>index.php/conductor/insertardb" class="form-horizontal" method="POST" enctype="multipart/form-data" id="formConductor">
				 
			 
			  
			  <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label">CI</label>
				<div class="col-sm-9">
				  <input type="text" class="form-control" id="ci" name="ci" maxlength="15"  required>
				  
				</div>
			  </div>
			  
			  <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label">Nombres</label>
				<div class="col-sm-9">
                <input type="text" class="form-control" id="nombres" name="nombres" maxlength="15"   required>
				</div>
              </div>
              <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label">Primer Apellido</label>
				<div class="col-sm-9">
                <input type="text" class="form-control" id="primerapellido" name="primerapellido" maxlength="15"  required >
				</div>
              </div>	
              <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label">Segundo Apellido</label>
				<div class="col-sm-9">
                <input type="text" class="form-control" id="segundoapellido" name="segundoapellido"  maxlength="15"   >
				</div>
              </div>	
              <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label">Direccion</label>
				<div class="col-sm-9">
                <input type="text" class="form-control" id="direccion" name="direccion" maxlength="15"  required>
				</div>
              </div>	
              <div class="form-group">
				<label for="titulo" class="col-sm-3 control-label">Telefono</label>
				<div class="col-sm-9">
                <input type="text" class="form-control" id="telefono" name="telefono" maxlength="8"  required >
				</div>
			  </div>	

			  <div class="form-group">
			  <div id='loader'></div>
			  <div class='outer_div'></div>
				<input type="hidden" value="conductor" id="enviarC">
				<div class="col-sm-offset-3 col-sm-9">
				  <button type="submit" class="btn btn-warning" id="enviarConductor"  >Registrar datos</button>
				</div>
			  </div>
			
			
			
			
		</div>
		<div class="col-md-5">
		 <h3 ><span class="	fas fa-file-image"></span> Fotografia</h3>
		 
		
		 
			<div class="form-group">
				
				<div class="col-sm-12">
				
				 
				 <div class="fileinput fileinput-new" data-provides="fileinput">
								  <div class="fileinput-new thumbnail" style="max-width: 100%;" >
									  <img class="img-rounded" src="<?=base_url()?>/bootstrap/images/user.png" style="max-width: 150px; max-height: 150px;"/>
								  </div>
								  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 250px; max-height: 250px;"></div>
								  <div>
									
									<span class="btn btn-warning btn-file"><span class="fileinput-new" name="newimage">Selecciona una imagen</span>
									<span class="fileinput-exists" onclick="upload_image();">Cambiar imagen</span><input type="file" name="fileToUpload" id="fileToUpload" required onchange="upload_image();"></span>
									<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Cancelar</a>
								  </div>
					</div>
					
					
				</div>
				
			  </div>



				<!--fotografia en vivo ->
				<div class="col-md-5">
		 <h3 ><span class="	fas fa-camera"></span> Capturar</h3>
		 
		
		 
			<div class="form-group">
				
				<div class="col-sm-12">
				
				 
				 <div class="fileinput fileinput-new" data-provides="fileinput">
								  <div id="camera"  style="max-width: 100%;" >
									 
								  </div>
								  <div  class="fileinput-preview fileinput-exists thumbnail" style="max-width: 250px; max-height: 250px;"></div>
								  <div>
									
									<span class="btn btn-warning btn-file"><span class="fileinput-new" name="newimage">Capturar</span>
									<span class="fileinput-exists" onclick="upload_image();">Cambiar imagen</span><input type="file" name="fileToUpload" id="fileToUpload" required onchange="upload_image();"></span>
									<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Cancelar</a>
								  </div>
					</div>
					
					
				</div>
				
			  </div-->
              
	
				<div id="alerta" class="col-sm-9">
            
						</div>
	  
			
			  
			 
			  
			  
         </form>
				 <!-- Custom scripts for validation Driver-->
  			 <script src="<?=base_url()?>/bootstrap/js/validarConductor.js"></script>
         <!--h1>Selecciona un dispositivo</h1>
	<div>
		<select name="listaDeDispositivos" id="listaDeDispositivos"></select>
		<button id="boton">Tomar foto</button>
		<button id="cancelar">Cancelar</button>
		<p id="estado"></p>
	</div>
	<br>
	<video muted="muted" id="video"></video>
	<canvas id="canvas" style="display: none;"></canvas-->
         
		</div>

    </div> 
    </div><!-- /container -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	
	
	<!-- script para cargar saycheese->
	<script src="<?=base_url()?>/bootstrap/js/say-cheese.js"></script>
	<script type="text/javascript">
		var sayCheese = new SayCheese('#camera', { 
			snapshots: true,
			widht:340,
			height:280
			
		});
		//sayCheese.start();
		

	

		sayCheese.takeSnapshot();
	
	</script-->
    


    
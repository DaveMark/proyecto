<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {
	
	public function index()
	{
		if ($this->session->userdata('txtUser') ) {
			redirect('Usuario/panel','refresh');
		}
		else
		{
            $this->load->view('login');
		}		
	}
	public function validarusuario(){

		$user=$_POST['txtUser'];
		$password=md5($_POST['txtPassword']);
		
		if(!$user){   //   Si no recibimos ningún valor proveniente del formulario, significa que el usuario recién ingresa.   
            $this->load->view('login');     //   Por lo tanto le presentamos la pantalla del formulario de ingreso.
		}

        else{
			$this->form_validation->set_rules('txtUser','e-mail','required|required'); //   Configuramos las validaciones ayudandonos con la librería form_validation del Framework Codeigniter
			$this->form_validation->set_rules('txtPassword','password','required');
			if(($this->form_validation->run()==FALSE)){            //   Verificamos si el usuario superó la validación
                $this->load->view('login');
			  }
			  else {

				$consulta=$this->login_model->validarusuarios($_POST['txtUser'],md5($_POST['txtPassword'])); 

				if($consulta->num_rows()>0){  
													
                foreach ($consulta->result() as $row) {					// La variable $consulta recibe resultado en la consultamos con el metodo result si es mayor a 0 
               	$this->session->set_userdata('txtUser',$row->login);
               	$this->session->set_userdata('tipo',$row->tipo);
              	redirect('usuario/panel','refresh');
                 }

					                                                                                    //   Si el usuario ingresó datos de acceso válido, imprimos un mensaje de validación exitosa en pantalla
				 }
				 else{   //   Si no logró validar
					redirect('usuario/index','refresh');
					//   Lo regresamos a la pantalla de login y pasamos como parámetro el mensaje de error a presentar en pantalla
				 }

			 }

		}			
	}
  public function panel()
    {
        if ($this->session->userdata('txtUser') ) {
            $this->load->view('head');
		    $this->load->view('index');
		    $this->load->view('footer');
          }
          else
          {
            redirect('usuario/index','refresh');

          }
    }


	public function logout() {
    
		// Elimina los datos de la sesión
		$this->session->sess_destroy();
		redirect('usuario/index','refresh');
 }
 public function administrar()
 {
	if ($this->session->userdata('txtUser') ) {
			
		$this->load->view('head');
		$data['usuarios']=$this->user_model->retornarusuarios();
		$this->load->view('users/user_administrar',$data);
		$this->load->view('footer');	}
	else
	{
					$this->load->view('login/login');
	}	
	 
	
 }


 public function modificar()
 {
	 $idUsuario=$_POST['txtidUsuario'];

	 $data['usuario']=$this->user_model->recuperarUsuario($idUsuario);
	 
	 $this->load->view('head');
	 $this->load->view('users/user_modificar',$data);
	 $this->load->view('footer');
 }
 public function modificardb()
 {

	$idUsuario=$_POST['txtidUser'];

	$usuario=$_POST['txtUsuario'];
	$tipo=$_POST['txtTipo'];
 
	
	
	$data['login']=$usuario;
	$data['tipo']=$tipo;
		 $this->user_model->modificarUsuario($idUsuario,$data);
		 
		 redirect('usuario/administrar','refresh');
	 

	 
 }
 public function adduser()
    {
			$this->load->view('head');
		    $this->load->view('users/user_anadir');
				$this->load->view('footer');
    }
 public function insertardb()
 {
	


		 $usuario=$_POST['txtUsuario'];
		 $password=md5($_POST['txtPassword']);
		 $tipo=$_POST['txtTipo'];
		
		 
		 
		 $data['login']=$usuario;
		 $data['password']=$password;
		 $data['tipo']=$tipo;
	
		 $this->user_model->insertarUsuario($data);
		 redirect('usuario/administrar','refresh');


	 }

	 public function eliminardb()
	 {
		 $id=$_POST['txtidUsuario'];
		 $data['estado']=0;
		 $this->user_model->eliminarUsuario($id,$data);
		 
		 redirect('usuario/administrar','refresh');
	 }
	 



}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehiculo extends CI_Controller {
	
	public function index()
	{
		
		$this->load->view('login');
		
	}
	public function principal()
	{
		
		$this->load->view('head');
		$this->load->view('index');
		$this->load->view('footer');
	}
	public function registrar()
	{
		
		$this->load->view('head');
		$this->load->view('Vehiculo/vehiculo_registrar');
		$this->load->view('footer');
	}
	public function administrar()
	{
		
		$this->load->view('head');
		$data['vehiculo']=$this->vehiculo_model->retornarVehiculo();
		$this->load->view('Vehiculo/vehiculo_administrar',$data);
		$this->load->view('footer');
	}
	public function modificar()
	{
		$idVehiculo=$_POST['idVehiculo'];
		$data['vehiculo']=$this->vehiculo_model->recuperarVehiculo($idVehiculo);
		
		$this->load->view('head');
		$this->load->view('Vehiculo/vehiculo_modificar',$data);
		$this->load->view('footer');
	}
	public function modificardb()
	{

		$config['upload_path']='./uploads/imagenes_conductor/';
		$config['allowed_types']='gif|jpg|png';
		$config['max_size']=1000;
		$config['max_width']=2024;
		$config['max_height']=2008;

		$this->load->library('upload',$config);

		if (!$this->upload->do_upload('fileToUpload')) {
			$idVehiculo=$_POST['idVehiculo'];
			$marca=$_POST['marca'];
			$modelo=$_POST['modelo'];
			$color=$_POST['color'];
			$placa=$_POST['placa'];
			$fabricacion=$_POST['fabricacion'];
			
			
			$data['marca']=$marca;
			$data['modelo']=$modelo;
			$data['color']=$color;
			$data['placa']=$placa;
			$data['anioFabricacion']=$fabricacion;
			$this->vehiculo_model->modificarVehiculo($idVehiculo,$data);
			
			redirect('vehiculo/administrar','refresh');

			
		}
		else {
			$file_info =$this->upload->data();
			$imagen = $file_info['file_name'];

			$idVehiculo=$_POST['idVehiculo'];
			$marca=$_POST['marca'];
			$modelo=$_POST['modelo'];
			$color=$_POST['color'];
			$placa=$_POST['placa'];
			$fabricacion=$_POST['fabricacion'];
			
			
			$data['marca']=$marca;
			$data['modelo']=$modelo;
			$data['color']=$color;
			$data['placa']=$placa;
			$data['anioFabricacion']=$fabricacion;
			$data['imagen']=$imagen;
			$this->vehiculo_model->modificarVehiculo($idVehiculo,$data);
			
			redirect('vehiculo/administrar','refresh');
		

		}
	}

	public function insertardb()
	{
		$config['upload_path']='./uploads/imagenes_vehiculo/';
		$config['allowed_types']='gif|jpg|png';
		$config['max_size']=1000;
		$config['max_width']=2024;
		$config['max_height']=2008;

        /*$files=count($_FILES['fileToUpload']['name']);
        for ($i=0; $i < $files; $i++) { 
            $_FILES['fileToUpload']['name']=$_FILES['fileToUpload']['name'][$i];
            $_FILES['fileToUpload']['type']=$_FILES['fileToUpload']['type'][$i];
            $_FILES['fileToUpload']['tmp_name']=$_FILES['fileToUpload']['tmp_name'][$i];
            $_FILES['fileToUpload']['error']=$_FILES['fileToUpload']['error'][$i];
            $_FILES['fileToUpload']['size']=$_FILES['fileToUpload']['size'][$i];
        }*/

		$this->load->library('upload',$config);

		if (!$this->upload->do_upload('fileToUpload')) {
			$error=array('error'=> $this->upload->display_errors());
			$this->load->view('index',$error);
		}
		else {
			$file_info =$this->upload->data();
			$imagen = $file_info['file_name'];
		


			$marca=$_POST['marca'];
			$modelo=$_POST['modelo'];
			$color=$_POST['color'];
			$placa=$_POST['placa'];
			$fabricacion=$_POST['fabricacion'];
			
			
			$data['marca']=$marca;
			$data['modelo']=$modelo;
			$data['color']=$color;
			$data['placa']=$placa;
			$data['anioFabricacion']=$fabricacion;
			$data['imagen']=$imagen;
			$this->vehiculo_model->insertarVehiculo($data);
			redirect('vehiculo/administrar','refresh');

		}
		
	}
	public function eliminardb()
	{
		$id=$_POST['idVehiculo'];
		$data['estado']=0;
		$this->vehiculo_model->eliminarVehiculo($id,$data);
		
		redirect('vehiculo/administrar','refresh');
	}
}

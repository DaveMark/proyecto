<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conductor extends CI_Controller {
	
	public function index()
	{
		
		$this->load->view('login');
		
	}
	public function principal()
	{
		
		$this->load->view('head');
		$this->load->view('index');
		$this->load->view('footer');
	}
	public function registrar()
	{
		
		$this->load->view('head');
		$this->load->view('Conductor/conductor_registrar');
		$this->load->view('footer');
	}
	public function administrar()
	{
		
		$this->load->view('head');
		$data['conductores']=$this->conductor_model->retornarConductor();
		$this->load->view('Conductor/conductor_administrar',$data);
		$this->load->view('footer');
	}
	public function modificar()
	{
		$idConductor=$_POST['idConductor'];
		$data['conductor']=$this->conductor_model->recuperarConductor($idConductor);
		
		$this->load->view('head');
		$this->load->view('Conductor/conductor_modificar',$data);
		$this->load->view('footer');
	}
	public function modificardb()
	{

		$config['upload_path']='./uploads/imagenes_conductor/';
		$config['allowed_types']='gif|jpg|png';
		$config['max_size']=1000;
		$config['max_width']=2024;
		$config['max_height']=2008;

		$this->load->library('upload',$config);
		if (!$this->upload->do_upload('fileToUpload')) {
			$idConductor=$_POST['idConductor'];
			$ci=$_POST['ci'];
			$nombres=$_POST['nombres'];
		 	$primerapellido=$_POST['primerapellido'];
			$segundoapellido=$_POST['segundoapellido'];
			$direccion=$_POST['direccion'];
			$telefono=$_POST['telefono'];

			$data['ci']=$ci;
			$data['nombres']=$nombres;
			$data['primerApellido']=$primerapellido;
			$data['segundoApellido']=$segundoapellido;
			$data['direccion']=$direccion;
			$data['telefono']=$telefono;
			$this->conductor_model->modificarConductor($idConductor,$data);
			
			redirect('conductor/administrar','refresh');
		}
		else {
			if (!$this->upload->do_upload('fileToUpload')) {
				$error=array('error'=> $this->upload->display_errors());
				$this->load->view('index',$error);
			}
			else {
				$file_info =$this->upload->data();
				$imagen = $file_info['file_name'];
	
				$idConductor=$_POST['idConductor'];
				$ci=$_POST['ci'];
				$nombres=$_POST['nombres'];
				 $primerapellido=$_POST['primerapellido'];
				$segundoapellido=$_POST['segundoapellido'];
				$direccion=$_POST['direccion'];
				$telefono=$_POST['telefono'];
	
				$data['ci']=$ci;
				$data['nombres']=$nombres;
				$data['primerApellido']=$primerapellido;
				$data['segundoApellido']=$segundoapellido;
				$data['direccion']=$direccion;
				$data['telefono']=$telefono;
				$data['imagen']=$imagen;
				$this->conductor_model->modificarConductor($idConductor,$data);
				
				redirect('conductor/administrar','refresh');
			
	
			}
		}

		
	}

	public function insertardb()
	{
		$config['upload_path']='./uploads/imagenes_conductor/';
		$config['allowed_types']='gif|jpg|png';
		$config['max_size']=1000;
		$config['max_width']=2024;
		$config['max_height']=2008;

		$this->load->library('upload',$config);

		if (!$this->upload->do_upload('fileToUpload')) {
			$error=array('error'=> $this->upload->display_errors());
			$this->load->view('index',$error);
		}
		else {
			$file_info =$this->upload->data();
			$imagen = $file_info['file_name'];
		


			$ci=$_POST['ci'];
			$nombres=$_POST['nombres'];
			$primerapellido=$_POST['primerapellido'];
			$segundoapellido=$_POST['segundoapellido'];
			$direccion=$_POST['direccion'];
			$telefono=$_POST['telefono'];
			
			$data['ci']=$ci;
			$data['nombres']=$nombres;
			$data['primerApellido']=$primerapellido;
			$data['segundoApellido']=$segundoapellido;
			$data['direccion']=$direccion;
			$data['telefono']=$telefono;
			$data['imagen']=$imagen;
			$this->conductor_model->insertarConductor($data);
			redirect('conductor/administrar','refresh');

		}
		
	}
	public function eliminardb()
	{
		$id=$_POST['idConductor'];
		$data['estado']=0;
		$this->conductor_model->eliminarConductor($id,$data);
		
		redirect('conductor/administrar','refresh');
	}
}

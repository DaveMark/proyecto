//Seleccionamos el formulario
var formularioConductor=document.getElementsByTagName("form")[0];
//Seleccionamos elementos del formulario
window.onload=iniciar;
function iniciar() {
    document.getElementById("enviarConductor").addEventListener('click',validarConductor,false);
    document.getElementById("enviarVehiculo").addEventListener('click',validarVehiculo,false);
    
}
function validarCi() {
    var elemento=document.getElementById("ci");
    document.getElementById("alerta").innerHTML="";
    var patron=/[\^$.*+?=!:|\\/()\[\]{}]/;
    if (patron.test(elemento.value)){
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo CI no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        elemento.focus();
        return false;
    }
    
    if (elemento.value=="" ) {
        
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo CI no puede ser vacio</div>';
        elemento.focus();
        return false;
    }
    if (isNaN(elemento.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo CI tiene que ser numerico</div>';
        elemento.focus();

        return false;
    }
    return true;
}

function validarNombre() {
    var elemento=document.getElementById("nombres");
    document.getElementById("alerta").innerHTML="";
    var patron=/[\^$.*+?=!:|\\/()\[\]{}]/;
    if (patron.test(elemento.value)){
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo nombre no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        elemento.focus();
        return false;
    }
    if (elemento.value=="" ) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo nombre no puede ser vacio</div>';
        elemento.focus();
        return false;
    }
    if (!isNaN(elemento.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo nombre solo acepta texto </div>';
        elemento.focus();
        return false;
    }
    return true;
}

function validarPrimerApellido() {
    var elemento=document.getElementById("primerapellido");
    document.getElementById("alerta").innerHTML="";
    var patron=/[\^$.*+?=!:|\\/()\[\]{}]/;
    if (patron.test(elemento.value)){
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Primer Apellido no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        elemento.focus();
        return false;
    }
    if (elemento.value=="" ) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo Primer Apellido no puede ser vacio</div>';
        elemento.focus();
        return false;
    }
    if (!isNaN(elemento.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo Primer Apellido solo acepta texto </div>';
        elemento.focus();
        return false;
    }
    return true;
}

function validarSegundoApellido() {
    var elemento=document.getElementById("segundoapellido");
    document.getElementById("alerta").innerHTML="";
    var patron=/[\^$.*+?=!:|\\/()\[\]{}]/;
    if (patron.test(elemento.value)){
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Segundo Apellido no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        elemento.focus();
        return false;
    }
    if (!isNaN(elemento.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo Segundo Apellido solo acepta texto </div>';
        elemento.focus();
        return false;
    }
    return true;
}

function validarDireccion() {
    var elemento=document.getElementById("direccion");
    document.getElementById("alerta").innerHTML="";
    var patron=/[\^$*+?=!:|\\/()\[\]{}]/;
    if (patron.test(elemento.value)){
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Direccion no puede tener caracteres especiales ^ $ * +? =! : |  / () [] {} </div>';
        elemento.focus();
        return false;
    }
    if (elemento.value=="" ) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo Direccion no puede ser vacio</div>';
        elemento.focus();
        return false;
    }
    if (!isNaN(elemento.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo Direccion solo acepta texto </div>';
        elemento.focus();
        return false;
    }
    return true;
}



function validarTelefono() {
    var elemento=document.getElementById("telefono");
    document.getElementById("alerta").innerHTML="";
    var patron=/[\^$.*+?=!:|\\/()\[\]{}]/;
    if (patron.test(elemento.value)){
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Telefono no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        elemento.focus();
        return false;
    }
    if (elemento.value=="" ) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo Telefono no puede ser vacio</div>';
        elemento.focus();
        return false;
    }
    if (isNaN(elemento.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo Telefono tiene que ser numerico</div>';
        elemento.focus();
        return false;
    }
    return true;
}

//metodos que permiten validar el formulario de Vehiculos
function validarMarca() {
    var element=document.getElementById("marca");
    var patron=/[\^$.*+?=:|\\/()\[\]{}]/;
    if (patron.test(element.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Marca no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        return false;
    }
    if (element.value=="") {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Marca no puede estar vacio </div>';
        return false;
    }
    if (!isNaN(element.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Marca solo acepta texto </div>';
        return false;
    }
    return true;
    
}

function validarModelo() {
    var element=document.getElementById("modelo");
    var patron=/[\^$.*+?=:;|\\/()\[\]{}]/;
    if (patron.test(element.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Modelo no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        return false;
    }
    if (element.value=="") {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Modelo no puede estar vacio</div>';
        return false;
    }
    if (!isNaN(element.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Modelo solo acepta texto </div>';
        return false;
    }
    return true;
    
}

function validarConductor(e) {

    if (validarCi() && validarNombre() && validarPrimerApellido() && validarSegundoApellido() && validarDireccion() && validarTelefono()) {
        document.getElementById("alerta").innerHTML="";
        return true;
    }
    else{
        e.preventDefault();
        return false;
    }
}

function validarVehiculo(e) {
    if (validarMarca() && validarModelo() ) {
        document.getElementById("alerta").innerHTML="";
        return true;
    }
    else{
        e.preventDefault();
        return false;
    }
    
}

function error(elemento) {
    elemento.className="error";
    elemento.focus();

}

function limpiarError(elemento) {
    elemento.className="";
}

//funcion que permite solo letas al momento de escribir en el input
function soloLetras(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key);
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
         if(key == especiales[i]){
             tecla_especial = true;
             break;
         }
     }

     if(letras.indexOf(tecla)==-1 && !tecla_especial){
         return false;
     }
 }
 //funcion que permite letras y numeros al momento de escribir en el input
 function direccion(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key);
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.0123456789";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
         if(key == especiales[i]){
             tecla_especial = true;
             break;
         }
     }

     if(letras.indexOf(tecla)==-1 && !tecla_especial){
         return false;
     }
 }
//funcion que solo numeros al momento de escribir en el input
function numero(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key);
    numeros = "0123456789";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
         if(key == especiales[i]){
             tecla_especial = true;
             break;
         }
     }

     if(numeros.indexOf(tecla)==-1 && !tecla_especial){
         return false;
     }
 }
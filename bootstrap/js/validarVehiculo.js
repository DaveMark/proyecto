//Seleccionamos el formulario
var formulario=document.getElementsByTagName("form")[0];
window.onload=iniciar;

function iniciar() {
    document.getElementById("enviarVehiculo").addEventListener('click',validarVehiculo,false);
}

//metodos que permiten validar el formulario de Vehiculos
function validarMarca() {
    var element=document.getElementById("marca");
    var patron=/[\^$.*+?=:|\\/()\[\]{}]/;
    if (patron.test(element.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Marca no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        element.focus();
        return false;
    }
    if (element.value=="") {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Marca no puede estar vacio </div>';
        element.focus();
        return false;
    }
    if (!isNaN(element.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Marca solo acepta texto </div>';
        element.focus();
        return false;
    }
    return true;
    
}

function validarModelo() {
    var element=document.getElementById("modelo");
    var patron=/[\^$.*+?=:;|\\/()\[\]{}]/;
    if (patron.test(element.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Modelo no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        element.focus();
        return false;
    }
    if (element.value=="") {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Modelo no puede estar vacio</div>';
        element.focus();
        return false;
    }
    if (!isNaN(element.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Modelo solo acepta texto </div>';
        element.focus();
        return false;
    }
    return true;
    
}
function validarColor() {
    var element=document.getElementById("color");
    var patron=/[\^$.*+?=:;|\\/()\[\]{}]/;
    if (patron.test(element.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Color no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        element.focus();
        return false;
    }
    if (element.value=="") {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Color no debe estar vacio</div>';
        element.focus();
        return false;
    }
    if (!isNaN(element.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Color solo recibe texto</div>';
        element.focus();
        return false;
        
    }
    return true;
}
function validarPlaca() {
    var element=document.getElementById("placa");
    var patron=/[\^$.*+?=:;|\\/()\[\]{}]/;
    if (patron.test(element.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Placa no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        element.focus();
        return false;
    }
    if (element.value=="") {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Placa no puede estar vacio </div>';
        element.focus();
        return false;
    }
    return true;
    
}
function validarFabricacion() {
    var elemento=document.getElementById("fabricacion");
    document.getElementById("alerta").innerHTML="";
    var patron=/[\^$.*+?=!:|\\/()\[\]{}]/;
    if (patron.test(elemento.value)){
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong> El campo Año de fabricacion no puede tener caracteres especiales ^ $. * +? =! : |  / () [] {} </div>';
        elemento.focus();
        return false;
    }
    if (elemento.value=="" ) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo Año de fabricacion no puede ser vacio</div>';
        elemento.focus();
        return false;
    }
    if (isNaN(elemento.value)) {
        document.getElementById("alerta").innerHTML='<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Atencion! </strong>El campo Año de fabricacion tiene que ser un año valido</div>';
        elemento.focus();
        return false;
    }
    return true;
}


function validarVehiculo(e) {
    if (validarMarca() && validarModelo() && validarColor() && validarPlaca() && validarFabricacion() ) {
        document.getElementById("alerta").innerHTML="";
        return true;
    }
    else{
        e.preventDefault();
        return false;
    }
    
}